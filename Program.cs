﻿using System;
using System.Collections.Generic;

class Program
{
   static void Main()
   {
      HashSet<int> set = new HashSet<int>();
      
      Console.Write("How many students for course A? ");
      int amount = int.Parse(Console.ReadLine());
      for (int i = 0; i < amount; i++)
      {
         set.Add(int.Parse(Console.ReadLine()));
      }

      Console.Write("How many students for course B? ");
      amount = int.Parse(Console.ReadLine());
      for (int i = 0; i < amount; i++)
      {
         set.Add(int.Parse(Console.ReadLine()));
      }

      Console.Write("How many students for course C? ");      
      amount = int.Parse(Console.ReadLine());
      for (int i = 0; i < amount; i++)
      {
         set.Add(int.Parse(Console.ReadLine()));
      }

      Console.WriteLine($"Total students: {set.Count}");
   }
}